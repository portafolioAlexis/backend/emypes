<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/precio', 'PrecioController@index')->name('precio');

//Route::middleware(['auth'])->group(function(){
	//rutas de administracion

	//index de administracion
	Route::get('admin', 'AdminController@index')->name('admin.index');
	// rutas de Roles
	Route::resource('rol','RolController', ['except'=>'show'])->middleware("permission:admin.rol");
	
	//Categorias productos
	Route::resource('categoria','CategoriaController', ['except'=>'show'])->middleware("permission:admin.categoria");
	Route::post('categoria/search','CategoriaController@search')->name('categoria.search')->middleware("permission:admin.categoria");
	
	//Empresa
	Route::resource('empresa','EmpresaController', ['except'=>'show'])->middleware("permission:admin.empresa");
	Route::post('empresa/search','EmpresaController@search')->name('empresa.search')->middleware("permission:admin.empresa");
	
	//Medidas
	Route::resource('medida','MedidaController', ['except'=>'show'])->middleware("permission:admin.medida");
	Route::post('medida/search','MedidaController@search')->name('medida.search')->middleware("permission:admin.medida");


//});