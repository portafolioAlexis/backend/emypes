<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class reqEmpresaMunicipios extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'empresa_id' => [
                'required',
                'numeric'
            ],
            'municipio_id' => [
                'required',
                'numeric'
            ]
        ];
    }

    public function messages()
    {
        return [
            'empresa_id.required' => 'El :attribute es requerido',
            'empresa_id.numeric' => 'El :attribute debe ser numerico',
            'municipio_id.required' => 'El :attribute es requerido',
            'municipio_id.numeric' => 'El :attribute debe ser numerico'
        ];
    }

    public function attributes()
    {
        return [
            'empresa_id' => 'Id de Empresa',
            'municipio_id' => 'Id de Municipio'
        ];
    }
}
