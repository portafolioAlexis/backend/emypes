<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class reqProductoMedida extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'producto_id' => [
                'required',
                'numeric'
            ],
            'medida_id' => [
                'required',
                'numeric'
            ]
        ];
    }

    public function messages()
    {
        return [
            'producto_id.required' => 'El :attribute es requerido',
            'producto_id.numeric' => 'El :attribute debe ser numerico',
            'medida_id.required' => 'El :attribute es requerido',
            'medida_id.numeric' => 'El :attribute debe ser numerico'
        ];
    }

    public function attributes()
    {
        return [
            'producto_id' => 'Id de Producto',
            'medida_id' => 'Id de Medida'
        ];
    }
}
