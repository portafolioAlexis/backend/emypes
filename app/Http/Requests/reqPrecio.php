<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class reqPrecio extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'unitario' => [
                'required',
                'regex:/^(?=.+)(?:[1-9]\d*|0)?(?:\.\d+)?$/'
            ],
            'estado' => [
                'required',
                'numeric'
            ],
            'max_venta' => 'numeric',
            'abastecimiento' => 'numeric',
            'producto_id' => [
                'required',
                'numeric'
            ],
            'medida_id' => [
                'required',
                'numeric'
            ],
            'empresa_id' => [
                'required',
                'numeric'
            ]
        ];
    }

    public function messages()
    {
        return [
            'unitario.required' => 'El :attribute es requerido',
            'unitario.regex' => 'El :attribute debe ser numerico',
            'estado.required' => 'El :attribute es requerido',
            'estado.numeric' => 'El :attribute debe ser numerico',
            'max_venta.numeric' => 'El :attribute debe ser numerico',
            'abastecimiento.numeric' => 'El :attribute debe ser numerico',
            'producto_id.required' => 'El :attribute es requerido',
            'producto_id.numeric' => 'El :attribute debe ser numericoo',
            'medida_id.required' => 'El :attribute es requerido',
            'medida_id.numeric' => 'El :attribute debe ser numerico',
            'empresa_id.required' => 'El :attribute es requerido',
            'empresa_id.numeric' => 'El :attribute debe ser numerico'
        ];
    }

    public function attributes()
    {
        return [
            'unitario' => 'Precio Unitario',
            'estado' => 'Estado',
            'max_venta' => 'Precio maximo de venta',
            'abastecimiento' => 'Abastecimiento',
            'producto_id' => 'Id de Producto',
            'medida_id' => 'Id de Medida',
            'empresa_id' => 'Id de Empresa'
        ];
    }
}
