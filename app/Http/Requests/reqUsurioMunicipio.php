<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class reqUsurioMunicipio extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'municipio_id' => [
                'required',
                'numeric'
            ],
            'user_id' => [
                'required',
                'numeric'
            ]
        ];
    }

    public function messages()
    {
        return [
            'municipio_id.required' => 'El :attribute es requerido',
            'municipio_id.numeric' => 'El :attribute debe ser numericoo',
            'user_id.required' => 'El :attribute es requerido',
            'user_id.numeric' => 'El :attribute debe ser numericoo'
        ];
    }

    public function attributes()
    {
        return [
            'municipio_id' => 'Id de Municipio',
            'user_id' => 'Id de Usuario'
        ];
    }
}
