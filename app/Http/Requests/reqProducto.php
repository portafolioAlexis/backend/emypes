<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class reqProducto extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'producto' => 'required',
            'max_permitido' => 'numeric',
            'estado' => [
                'required',
                'numeric'
            ],
            'categoria_id' => [
                'required',
                'numeric'
            ]
        ];
    }

    public function messages()
    {
        return [
            'producto.required' => 'El :attribute es requerido',
            'max_permitido.numeric' => 'El :attribute debe ser numerico',
            'estado.required' => 'El :attribute es requerido',
            'estado.numeric' => 'El :attribute debe ser numerico',
            'categoria_id.required' => 'El :attribute es requerido',
            'categoria_id.numeric' => 'El :attribute debe ser numericoo'
        ];
    }

    public function attributes()
    {
        return [
            'producto' => 'Nombre Producto',
            'max_permitido' => 'Maximo permitido',
            'estado' => 'Estado',
            'categoria_id' => 'Id de Categoria'
        ];
    }
}
