<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class reqDetalleCarro extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'cantidad' => [
                'required',
                'numeric'
            ],
            'descuento' => [
                'required',
                'regex:/^(?=.+)(?:[1-9]\d*|0)?(?:\.\d+)?$/'
            ],
            'estado' => [
                'required',
                'numeric'
            ],
            'agotado' => [
                'required',
                'numeric'
            ],
            'precio_id' => [
                'required',
                'numeric'
            ]
        ];
    }

    public function messages()
    {
        return [
            'cantidad.required' => 'El :attribute es requerido',
            'cantidad.numeric' => 'El :attribute debe ser numerico',
            'descuento.required' => 'El :attribute es requerido',
            'descuento.regex' => 'El :attribute debe ser numerico',
            'estado.required' => 'El :attribute es requerido',
            'estado.numeric' => 'El :attribute debe ser numerico',
            'agotado.required' => 'El :attribute es requerido',
            'agotado.numeric' => 'El :attribute debe ser numerico',
            'precio_id.required' => 'El :attribute es requerido',
            'precio_id.numeric' => 'El :attribute debe ser numerico'
        ];
    }

    public function attributes()
    {
        return [
            'cantidad' => 'Cantidad',
            'descuento' => 'Descuento',
            'estado' => 'Estado',
            'agotado' => 'Agotado',
            'precio_id' => 'Id de Precio',
        ];
    }
}
