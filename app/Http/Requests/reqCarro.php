<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class reqCarro extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'estado_compra' => [
                'required',
                'numeric'
            ],
            'estado_empresa' => [
                'required',
                'numeric'
            ],
            'mensaje_empresa' => 'required',
            'entrega' => [
                'required',
                'date_format:m/d/Y'
            ],
            'hora' => [
                'required',
                'date_format:H:i:s'
            ],
            'empresa_id' => [
                'required',
                'numeric'
            ],
            'user_id' => [
                'required',
                'numeric'
            ]
        ];
    }

    public function messages()
    {
        return [
            'estado_compra.required' => 'El :attribute es requerido',
            'estado_compra.numeric' => 'El :attribute debe ser numerico',
            'estado_empresa.required' => 'El :attribute es requerido',
            'estado_empresa.numeric' => 'El :attribute debe ser numerico',
            'mensaje_empresa.required' => 'El :attribute es requerido',
            'entrega.required' => 'El :attribute es requerido',
            'entrega.date_format' => 'El formato :attribute es incorrecto',
            'hora.required' => 'El :attribute es requerido',
            'hora.date_format' => 'El formato :attribute es incorrecto',
            'empresa_id.required' => 'El :attribute es requerido',
            'empresa_id.numeric' => 'El :attribute debe ser numerico',
            'user_id.required' => 'El :attribute es requerido',
            'user_id.numeric' => 'El :attribute debe ser numerico',
        ];
    }

    public function attributes()
    {
        return [
            'estado_compra' => 'Estado de Compra',
            'estado_empresa' => 'Estado de Empresa',
            'entrega' => 'Entrega',
            'hora' => 'Hora',
            'empresa_id' => 'Id de Empresa',
            'user_id' => 'Id de Usuario',
        ];
    }
}
