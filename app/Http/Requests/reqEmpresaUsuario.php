<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class reqEmpresaUsuario extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'estado' => [
                'required',
                'numeric'
            ],
            'empresa_id' => [
                'required',
                'numeric'
            ],
            'user_id' => [
                'required',
                'numeric'
            ]
        ];
    }

    public function messages()
    {
        return [
            'estado.required' => 'El :attribute es requerido',
            'estado.numeric' => 'El :attribute debe ser numerico',
            'empresa_id.required' => 'El :attribute es requerido',
            'empresa_id.numeric' => 'El :attribute debe ser numerico',
            'user_id.required' => 'El :attribute es requerido',
            'user_id.numeric' => 'El :attribute debe ser numerico',
        ];
    }

    public function attributes()
    {
        return [
            'estado' => 'Estado',
            'empresa_id' => 'Id de Empresa',
            'user_id' => 'Id de Usuario'
        ];
    }
}
