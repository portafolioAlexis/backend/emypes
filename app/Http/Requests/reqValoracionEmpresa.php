<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class reqValoracionEmpresa extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'valoracion' => [
                'required',
                'numeric'
            ],
            'comentario' => 'required',
            'empresa_id' => [
                'required',
                'numeric'
            ],
            'carro_id' => [
                'required',
                'numeric'
            ],
            'user_id' => [
                'required',
                'numeric'
            ]
        ];
    }

    public function messages()
    {
        return [
            'valoracion.required' => 'El :attribute es requerido',
            'valoracion.numeric' => 'El :attribute debe ser numericoo',
            'comentario.required' => 'El :attribute es requerido',
            'empresa_id.required' => 'El :attribute es requerido',
            'empresa_id.numeric' => 'El :attribute debe ser numericoo',
            'carro_id.required' => 'El :attribute es requerido',
            'carro_id.numeric' => 'El :attribute debe ser numericoo',
            'user_id.required' => 'El :attribute es requerido',
            'user_id.numeric' => 'El :attribute debe ser numericoo'
        ];
    }

    public function attributes()
    {
        return [
            'valoracion' => 'Valoracion',
            'comentario' => 'Comentario',
            'empresa_id' => 'Id de Empresa',
            'carro_id' => 'Id de Carro',
            'user_id' => 'Id de Usuario'
        ];
    }
}
