<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class reqProductoSug extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'sugerencia' => 'required',
            'votos' => [
                'required',
                'numeric'
            ],
            'estado' => [
                'required',
                'numeric'
            ],
            'seguimiento' => [
                'required',
                'numeric'
            ],
            'user_id' => [
                'required',
                'numeric'
            ]
        ];
    }

    public function messages()
    {
        return [
            'sugerencia.required' => 'El :attribute es requerido',
            'votos.required' => 'El :attribute es requerido',
            'votos.numeric' => 'El :attribute debe ser numerico',
            'estado.required' => 'El :attribute es requerido',
            'estado.numeric' => 'El :attribute debe ser numerico',
            'seguimiento.required' => 'El :attribute es requerido',
            'seguimiento.numeric' => 'El :attribute debe ser numericoo',
            'user_id.required' => 'El :attribute es requerido',
            'user_id.numeric' => 'El :attribute debe ser numericoo'
        ];
    }

    public function attributes()
    {
        return [
            'sugerencia' => 'Sugerencia',
            'votos' => 'Votos',
            'estado' => 'Estado',
            'seguimiento' => 'Seguimiento',
            'user_id' => 'Id de Usuario'
        ];
    }
}
