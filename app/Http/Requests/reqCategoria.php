<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class reqCategoria extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'categoria' => 'required',
            'source' => 'required',
            'tipo' => [
                'required',
                'numeric'
            ]
        ];
    }

    public function messages()
    {
        return [
            'categoria.required' => 'El :attribute es requerido',
            'source.required' => 'El :attribute es requerido',
            'tipo.required' => 'El :attribute es requerido',
            'tipo.numeric' => 'El :attribute debe ser numerico'
        ];
    }

    public function attributes()
    {
        return [
            'categoria' => 'Categoria',
            'source' => 'Source',
            'tipo' => 'Tipo'
        ];
    }
}
