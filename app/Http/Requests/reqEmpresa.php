<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class reqEmpresa extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nombre_empresa' => 'required',
            'direccion' => 'required',
            'descripcion' => 'required',
            'coordenadas' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'nombre_empresa.required' => 'El :attribute es requerido',
            'direccion.required' => 'El :attribute es requerido',
            'descripcion.required' => 'El :attribute es requerido',
            'coordenadas.required' => 'El :attribute es requerido'
        ];
    }

    public function attributes()
    {
        return [
            'nombre_empresa' => 'Nombre Empresa',
            'direccion' => 'Direccion',
            'descripcion' => 'Descripcion',
            'coordenadas' => 'Coordenadas'
        ];
    }
}
