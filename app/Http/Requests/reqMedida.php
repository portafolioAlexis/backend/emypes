<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class reqMedida extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'medida' => 'required',
            'sufijo' => 'required',
            'icono' => 'required'
        ];
    }

    public function messages()
    {
        return [
            'medida.required' => 'El :attribute es requerido',
            'sufijo.required' => 'El :attribute es requerido',
            'icono.required' => 'El :attribute es requerido'
        ];
    }

    public function attributes()
    {
        return [
            'medida' => 'Medida',
            'sufijo' => 'Sufijo',
            'icono' => 'Icono'
        ];
    }
}
