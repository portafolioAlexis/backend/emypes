<?php

namespace App\Http\Controllers;

use App\medida;
use Illuminate\Http\Request;

class MedidaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\medida  $medida
     * @return \Illuminate\Http\Response
     */
    public function show(medida $medida)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\medida  $medida
     * @return \Illuminate\Http\Response
     */
    public function edit(medida $medida)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\medida  $medida
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, medida $medida)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\medida  $medida
     * @return \Illuminate\Http\Response
     */
    public function destroy(medida $medida)
    {
        //
    }
}
