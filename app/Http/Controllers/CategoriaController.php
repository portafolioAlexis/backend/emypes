<?php

namespace App\Http\Controllers;

use App\categoria;
use Illuminate\Http\Request;

class CategoriaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    private $table = 'categoria';
    public function index()
    {

        return view($this->table.'.index', [
            'table' =>  $this->table, 
            'title'=>'Listado de categorias',
            'data'=> categoria::paginate(10)
            ]);
    }
    public function search(Request $r)
    {
        if(!isset($r->txtBq) || strlen(trim($r->txtBq)) == 0)
            return redirect()->back()->with('danger', 'Debe llenar el campo para buscar');
        return view($this->table.'.index', [
            'table' =>  $this->table, 
            'title'=>'Listado de categorias',
            'data'=> categoria::where('categoria', 'like', '')->paginate()
            ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view($this->table.'.create', ['table' =>  $this->table, 'title'=>'Agregar categoria']);
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        return redirect()->route($this->table.'.index')->with('danger', 'Guardado con exito');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\categoria  $categoria
     * @return \Illuminate\Http\Response
     */
    public function show(categoria $categoria)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\categoria  $categoria
     * @return \Illuminate\Http\Response
     */
    public function edit(categoria $categoria)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\categoria  $categoria
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, categoria $categoria)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\categoria  $categoria
     * @return \Illuminate\Http\Response
     */
    public function destroy(categoria $categoria)
    {
        //
    }
}
