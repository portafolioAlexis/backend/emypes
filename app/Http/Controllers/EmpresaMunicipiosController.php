<?php

namespace App\Http\Controllers;

use App\empresa_municipios;
use Illuminate\Http\Request;

class EmpresaMunicipiosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\empresa_municipios  $empresa_municipios
     * @return \Illuminate\Http\Response
     */
    public function show(empresa_municipios $empresa_municipios)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\empresa_municipios  $empresa_municipios
     * @return \Illuminate\Http\Response
     */
    public function edit(empresa_municipios $empresa_municipios)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\empresa_municipios  $empresa_municipios
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, empresa_municipios $empresa_municipios)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\empresa_municipios  $empresa_municipios
     * @return \Illuminate\Http\Response
     */
    public function destroy(empresa_municipios $empresa_municipios)
    {
        //
    }
}
