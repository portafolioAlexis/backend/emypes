<?php

namespace App\Http\Controllers;

use App\valoracion_empresa;
use Illuminate\Http\Request;

class ValoracionEmpresaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\valoracion_empresa  $valoracion_empresa
     * @return \Illuminate\Http\Response
     */
    public function show(valoracion_empresa $valoracion_empresa)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\valoracion_empresa  $valoracion_empresa
     * @return \Illuminate\Http\Response
     */
    public function edit(valoracion_empresa $valoracion_empresa)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\valoracion_empresa  $valoracion_empresa
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, valoracion_empresa $valoracion_empresa)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\valoracion_empresa  $valoracion_empresa
     * @return \Illuminate\Http\Response
     */
    public function destroy(valoracion_empresa $valoracion_empresa)
    {
        //
    }
}
