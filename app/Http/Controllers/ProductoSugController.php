<?php

namespace App\Http\Controllers;

use App\producto_sug;
use Illuminate\Http\Request;

class ProductoSugController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\producto_sug  $producto_sug
     * @return \Illuminate\Http\Response
     */
    public function show(producto_sug $producto_sug)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\producto_sug  $producto_sug
     * @return \Illuminate\Http\Response
     */
    public function edit(producto_sug $producto_sug)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\producto_sug  $producto_sug
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, producto_sug $producto_sug)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\producto_sug  $producto_sug
     * @return \Illuminate\Http\Response
     */
    public function destroy(producto_sug $producto_sug)
    {
        //
    }
}
