<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\rol;
class RolController extends Controller
{
    public function index()
    {
    	//return rol::all();
    	return view('rol.index', ['title'=>'Roles de usuarios', 'table'=>'rol']);
    }
    public function search(Request $r)
    {
    	if (isset($r->txtBq)) 
    		return rol::orWhere('name', 'like', $r->txtBq)->orWhere('description', 'like', $r->txtBq)->get();
    	else return null;
    }

    public function create(Request $r){
        return null;
    }
}
