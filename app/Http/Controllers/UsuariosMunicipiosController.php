<?php

namespace App\Http\Controllers;

use App\usuarios_municipios;
use Illuminate\Http\Request;

class UsuariosMunicipiosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\usuarios_municipios  $usuarios_municipios
     * @return \Illuminate\Http\Response
     */
    public function show(usuarios_municipios $usuarios_municipios)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\usuarios_municipios  $usuarios_municipios
     * @return \Illuminate\Http\Response
     */
    public function edit(usuarios_municipios $usuarios_municipios)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\usuarios_municipios  $usuarios_municipios
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, usuarios_municipios $usuarios_municipios)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\usuarios_municipios  $usuarios_municipios
     * @return \Illuminate\Http\Response
     */
    public function destroy(usuarios_municipios $usuarios_municipios)
    {
        //
    }
}
