<?php

namespace App\Http\Controllers;

use App\detalle_carro;
use Illuminate\Http\Request;

class DetalleCarroController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\detalle_carro  $detalle_carro
     * @return \Illuminate\Http\Response
     */
    public function show(detalle_carro $detalle_carro)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\detalle_carro  $detalle_carro
     * @return \Illuminate\Http\Response
     */
    public function edit(detalle_carro $detalle_carro)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\detalle_carro  $detalle_carro
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, detalle_carro $detalle_carro)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\detalle_carro  $detalle_carro
     * @return \Illuminate\Http\Response
     */
    public function destroy(detalle_carro $detalle_carro)
    {
        //
    }
}
