<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class precio extends Model
{
    //
    public function producto(){
        return $this->belongsTo(producto::class);
    }
    public function medida(){
        return $this->belongsTo(medida::class);
    }
    public function empresa(){
        return $this->belongsTo(empresa::class);
    }
}
