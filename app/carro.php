<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class carro extends Model
{
    public function empresa(){
        return $this->belongsTo(empresa::class);
    }
    public function user(){
        return $this->belongsTo(User::class);
    }
}
