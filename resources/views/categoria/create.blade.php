@extends('layouts.form')
@section('form')
<!-- En el atributo: action solo cambiar la palabra  store / update  
  si es creacion se usa store, si es edicion update
  Tambien cambiar el id del formulario y de cada campo para que funcione materialize. 
-->
<form action="{{ route($table.'.store') }}" id="formAddCategoria">
      
      <!-- Cambiar el formulario segun los campos necesarios desde aqui -->
      <div class="input-field col s12">
        <input type="text" id="first_name" class="validate" name="">
        <label for="first_name">Categoria</label>
      </div>
      <div class="input-field col s12">
        <select name="tipo" v-model='tipo' name="">
          <option value="" disabled selected>Elija una opcion</option>
          <option value="1">Imagen</option>
          <option value="2">Video</option>
          <option value="3">Icono</option>
        </select>
        <label>Tipo de archivo</label>
      </div>
      <div class="col s12" v-show='tipo > 0'>
        Presione encima para agregar archivo
      </div>
      <div class="input-field col s12" v-show='tipo == 1'>
        <input type="file" id="imagen" class="validate" style="display:none;" name="">
        <label for="imagen"><i class="material-icons large">image</i></label>
      </div>
      <div class="input-field col s12"  v-show='tipo == 2'>
        <input type="file" id="video" class="validate" style="display:none;" name="">
        <label for="video"><i class="material-icons large">ondemand_video</i></label>
      </div>

      <!-- Cambiar hasta aqui lo demas es igual -->
      <div class="col s12  btns">
        <a href="{{ URL::previous() }}" class="btn red darken-1">Volver</a>
        <button class="btn grey darken-1" type="submit">Guardar</button>
      </div>
    </form>
@endsection
<!--Esto no es necesario en todos los formularios es codigo vue-->
@section('scripts')
    <script>
      document.addEventListener('DOMContentLoaded', function() {
        var elems = document.querySelectorAll('select');
        var instances = M.FormSelect.init(elems);
      });
      new Vue({
        el:'#formAddCategoria',
        data:{
          tipo:0,
        }
      });
    </script>
@endsection