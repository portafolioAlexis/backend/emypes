@extends('layouts.admin')
@section('content')
	<div class="container">
		<div class="row">
			<div class="col s12">
				<form action="{{ route($table.'.search') }}" method="POST">
					<div class="input-field col l11">
						<input type="text" id="search" class="validate" required name="txtSearch">
						<label for="search">Buscar</label>
					</div>
					<div class="input-field col l1">
						<button type="submit" class="btn indigo darker-2">Buscar</button>
					</div>
				</form>
			</div>
			<div class="col s12">
				<a href="{{ route($table.'.create') }}" class="btn"><i class="material-icons left">add</i> Agregar</a>
			</div>
			
			
				@if ((session('danger') && session('danger') != "") || (session('info') && session('info') != ""))
					<div class="col s12">
						<div class="alert white-text {{ (session('danger'))?'amber darken-4':'green accent-2' }}" >
							{{ (session('danger'))?session('danger'):session('info') }}
						</div>
					</div>
				@endif
				
				
			
			<div class="col s12">
				@yield('list')
			</div>
		</div>
	</div>
@endsection
