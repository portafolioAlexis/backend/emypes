<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
class DepartamentosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('departamentos')->insert(["Departamento"=>"Ahuachapán"]);
		DB::table('departamentos')->insert(["Departamento"=>"Santa Ana"]);
		DB::table('departamentos')->insert(["Departamento"=>"Sonsonate"]);
		DB::table('departamentos')->insert(["Departamento"=>"La Libertad"]);
		DB::table('departamentos')->insert(["Departamento"=>"Chalatenango"]);
		DB::table('departamentos')->insert(["Departamento"=>"San Salvador"]);
		DB::table('departamentos')->insert(["Departamento"=>"Cuscatlán"]);
		DB::table('departamentos')->insert(["Departamento"=>"La Paz"]);
		DB::table('departamentos')->insert(["Departamento"=>"Cabañas"]);
		DB::table('departamentos')->insert(["Departamento"=>"San Vicente"]);
		DB::table('departamentos')->insert(["Departamento"=>"Usulután"]);
		DB::table('departamentos')->insert(["Departamento"=>"Morazán"]);
		DB::table('departamentos')->insert(["Departamento"=>"San Miguel"]);
		DB::table('departamentos')->insert(["Departamento"=>"La Unión"]);
    }
}
