<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCarrosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('carros', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->enum('estado_compra', ['Creando lista de compra','Solicitando', 'Comprado', 'No comprado']);
            $table->enum('estado_empresa', ['Aun sin ver el pedido','Procesando...', 'Pedido enviado', 'No se puede entregar']);
            $table->string('mensaje_empresa')->nullable();
            $table->date('entrega')->nullable();
            $table->time('hora')->nullable();
            $table->bigInteger('empresa_id')->unsigned();
            $table->foreign('empresa_id')->references('id')->on('empresas');
            $table->bigInteger('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('carros');
    }
}
