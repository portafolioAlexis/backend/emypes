<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductoSugsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('producto_sugs', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('sugerencia');
            $table->integer('votos');
            $table->boolean('estado');
            $table->enum('seguimiento', ['Procesando...', 'Negado', 'Ya estaba habilitado', 'Aceptado']);
            $table->bigInteger('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('producto_sugs');
    }
}
